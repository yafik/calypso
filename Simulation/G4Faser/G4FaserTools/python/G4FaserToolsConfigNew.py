# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
from __future__ import print_function
from AthenaConfiguration.ComponentFactory import CompFactory


from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaCommon import CfgMgr
SensitiveDetectorMasterTool=CompFactory.SensitiveDetectorMasterTool

from VetoG4_SD.VetoG4_SDToolConfig import VetoSensorSDCfg
from TriggerG4_SD.TriggerG4_SDToolConfig import TriggerSensorSDCfg
from PreshowerG4_SD.PreshowerG4_SDToolConfig import PreshowerSensorSDCfg
from FaserSCT_G4_SD.FaserSCT_G4_SDToolConfig import SctSensorSDCfg

def generateScintSensitiveDetectorList(ConfigFlags):

    result = ComponentAccumulator()
    SensitiveDetectorList=[]

    if ConfigFlags.Detector.SimulateVeto:
        accVeto,toolVeto = VetoSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolVeto ]
        result.merge(accVeto)
    
    if ConfigFlags.Detector.SimulateTrigger:
        accTrigger,toolTrigger = TriggerSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolTrigger ]
        result.merge(accTrigger)
    
    if ConfigFlags.Detector.SimulatePreshower:
        accPreshower,toolPreshower = PreshowerSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolPreshower ]
        result.merge(accPreshower)
    
    return result, SensitiveDetectorList #List of tools here now! (CALL IT TOOL LIST?)

def generateTrackerSensitiveDetectorList(ConfigFlags):

    result = ComponentAccumulator()
    SensitiveDetectorList=[]

    if ConfigFlags.Detector.SimulateFaserSCT:
        accSCT,toolSCT = SctSensorSDCfg(ConfigFlags)
        SensitiveDetectorList += [ toolSCT ]
        result.merge(accSCT)
        
    return result, SensitiveDetectorList #List of tools here now! (CALL IT TOOL LIST?)


# def generateEnvelopeSensitiveDetectorList(ConfigFlags):
#     SensitiveDetectorList=[]
#     if ConfigFlags.Beam.Type == 'cosmics' and not ConfigFlags.Sim.ReadTR:
#         SensitiveDetectorList+=['CosmicRecord']
#     return SensitiveDetectorList

def generateSensitiveDetectorList(ConfigFlags):
    result = ComponentAccumulator()
    SensitiveDetectorList=[]
    # SensitiveDetectorList += generateEnvelopeSensitiveDetectorList(ConfigFlags) # to update

    acc_ScintSensitiveDetector, ScintSensitiveDetectorList = generateScintSensitiveDetectorList(ConfigFlags)
    SensitiveDetectorList += ScintSensitiveDetectorList

    acc_TrackerSensitiveDetector, TrackerSensitiveDetectorList = generateTrackerSensitiveDetectorList(ConfigFlags)
    SensitiveDetectorList += TrackerSensitiveDetectorList

    result.merge(acc_ScintSensitiveDetector)
    result.merge(acc_TrackerSensitiveDetector)

    result.setPrivateTools(SensitiveDetectorList)
    return result

def SensitiveDetectorMasterToolCfg(ConfigFlags, name="SensitiveDetectorMasterTool", **kwargs):
    result = ComponentAccumulator()
    accSensitiveDetector = generateSensitiveDetectorList(ConfigFlags)
    kwargs.setdefault("SensitiveDetectors", result.popToolsAndMerge(accSensitiveDetector)) #list of tools

    result.addPublicTool(SensitiveDetectorMasterTool(name, **kwargs)) #note -this is still a public tool
    return result

def getEmptySensitiveDetectorMasterTool(name="EmptySensitiveDetectorMasterTool", **kwargs):
    return CfgMgr.SensitiveDetectorMasterTool(name, **kwargs)
