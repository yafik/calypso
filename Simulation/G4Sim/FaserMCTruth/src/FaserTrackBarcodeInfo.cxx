/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserMCTruth/FaserTrackBarcodeInfo.h"

FaserTrackBarcodeInfo::FaserTrackBarcodeInfo(int bc, const ISF::FaserISFParticle* baseIsp):FaserVTrackInformation(BarcodeOnly),m_theBaseISFParticle(baseIsp),m_barcode(bc),m_returnedToISF(false)
{
}

int FaserTrackBarcodeInfo::GetParticleBarcode() const
{
  return m_barcode;
}

void FaserTrackBarcodeInfo::SetBaseISFParticle(const ISF::FaserISFParticle* isp)
{
  m_theBaseISFParticle=isp;
}

const ISF::FaserISFParticle* FaserTrackBarcodeInfo::GetBaseISFParticle() const
{
  return m_theBaseISFParticle;
}

void FaserTrackBarcodeInfo::SetReturnedToISF(bool returned)
{
  m_returnedToISF = returned;
}

bool FaserTrackBarcodeInfo::GetReturnedToISF() const
{
  return m_returnedToISF;
}
