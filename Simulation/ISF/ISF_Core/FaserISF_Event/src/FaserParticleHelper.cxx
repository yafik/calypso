/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// FaserParticleHelper.cxx, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

// class include
#include "FaserISF_Event/FaserParticleHelper.h"

// HepMC includes
#include "AtlasHepMC/GenParticle.h"
#include "AtlasHepMC/Flow.h"
#include "AtlasHepMC/SimpleVector.h" // HepMC::FourVector

// ISF includes
#include "FaserISF_Event/FaserISFParticle.h"

HepMC::GenParticle* ISF::FaserParticleHelper::convert( const ISF::FaserISFParticle &particle) {

  const Amg::Vector3D &mom = particle.momentum();
  double mass = particle.mass();
  double energy = sqrt( mom.mag2() + mass*mass);
  HepMC::FourVector fourMomentum( mom.x(), mom.y(), mom.z(), energy);
  int status = 1; // stable particle not decayed by EventGenerator

  auto* hepParticle = new HepMC::GenParticle( fourMomentum, particle.pdgCode(), status );
  hepParticle->suggest_barcode( particle.barcode() );

  // return a newly created GenParticle
  return hepParticle;
}

