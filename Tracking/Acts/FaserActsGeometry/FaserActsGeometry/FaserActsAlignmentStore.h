/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FASERACTSGEOMETRY_ACTSALIGNMENTSTORE_H
#define FASERACTSGEOMETRY_ACTSALIGNMENTSTORE_H

#include "GeoModelUtilities/GeoAlignmentStore.h"
#include "GeoModelUtilities/TransformMap.h"
#include "AthenaKernel/CLASS_DEF.h"
#include "AthenaKernel/CondCont.h"

#include "Acts/Utilities/Definitions.hpp"

#include <stdexcept>

class FaserActsDetectorElement;

class FaserActsAlignmentStore : public GeoAlignmentStore
{
  public:
    FaserActsAlignmentStore() {}
    FaserActsAlignmentStore(const GeoAlignmentStore& gas);

    void setTransform(const FaserActsDetectorElement* key, const Acts::Transform3D&);
    const Acts::Transform3D* getTransform(const FaserActsDetectorElement* key) const;

    void append(const GeoAlignmentStore& gas);

  private:
    TransformMap<FaserActsDetectorElement, Acts::Transform3D> m_transforms;
};

CLASS_DEF(FaserActsAlignmentStore, 58650257, 1)
CONDCONT_DEF( FaserActsAlignmentStore , 124731561 );

#endif
