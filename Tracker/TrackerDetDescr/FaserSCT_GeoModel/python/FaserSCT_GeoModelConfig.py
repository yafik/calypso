# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentFactory import CompFactory
from IOVDbSvc.IOVDbSvcConfig import addFoldersSplitOnline

# def getFaserSCT_DetectorTool(name="FaserSCT_DetectorTool", **kwargs):
#     kwargs.setdefault("DetectorName",     "SCT")
#     kwargs.setdefault("Alignable",        True)
#     kwargs.setdefault("RDBAccessSvc",     "RDBAccessSvc")
#     kwargs.setdefault("GeometryDBSvc",    "TrackerGeometryDBSvc")
#     kwargs.setdefault("GeoDbTagSvc",      "GeoDbTagSvc")
#     FaserSCT_DetectorTool = CompFactory.FaserSCT_DetectorTool
#     return FaserSCT_DetectorTool(name, **kwargs)

def FaserSCT_GeometryCfg( flags ):
    from FaserGeoModel.GeoModelConfig import GeoModelCfg
    acc = GeoModelCfg( flags )
    geoModelSvc=acc.getPrimary()
    
    GeometryDBSvc = CompFactory.GeometryDBSvc
    acc.addService(GeometryDBSvc("TrackerGeometryDBSvc"))
    
    # from RDBAccessSvc.RDBAccessSvcConf import RDBAccessSvc
    # acc.addService(RDBAccessSvc("RDBAccessSvc"))

    # from DBReplicaSvc.DBReplicaSvcConf import DBReplicaSvc
    # acc.addService(DBReplicaSvc("DBReplicaSvc"))

    FaserSCT_DetectorTool = CompFactory.FaserSCT_DetectorTool
    sctDetectorTool = FaserSCT_DetectorTool()

    sctDetectorTool.useDynamicAlignFolders = flags.GeoModel.Align.Dynamic
    geoModelSvc.DetectorTools += [ sctDetectorTool ]
    
    if flags.GeoModel.Align.Dynamic:
        # acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL1/ID","/Indet/AlignL1/ID",className="CondAttrListCollection"))
        # acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL2/SCT","/Indet/AlignL2/SCT",className="CondAttrListCollection"))
        # acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL3","/Indet/AlignL3",className="AlignableTransformContainer"))
        print("FaserSCT dynamic align flag is not supported!")
    else:
        if (not flags.Detector.SimulateFaserSCT) or flags.Detector.OverlaySCT:
            acc.merge(addFoldersSplitOnline(flags,"SCT","/Tracker/Onl/Align","/Tracker/Align",className="AlignableTransformContainer"))
        # else:
        #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align"))
    if flags.Common.Project != "AthSimulation": # Protection for AthSimulation builds
        if (not flags.Detector.SimulateFaserSCT) or flags.Detector.OverlaySCT:
            FaserSCT_AlignCondAlg = CompFactory.FaserSCT_AlignCondAlg
            sctAlignCondAlg = FaserSCT_AlignCondAlg(name = "FaserSCT_AlignCondAlg",
                                                    UseDynamicAlignFolders = flags.GeoModel.Align.Dynamic)
            acc.addCondAlgo(sctAlignCondAlg)
            FaserSCT_DetectorElementCondAlg = CompFactory.FaserSCT_DetectorElementCondAlg
            sctDetectorElementCondAlg = FaserSCT_DetectorElementCondAlg(name = "FaserSCT_DetectorElementCondAlg")
            acc.addCondAlgo(sctDetectorElementCondAlg)
    return acc
