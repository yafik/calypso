/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

#include "ScintWaveformContainerCnv.h"

ScintWaveformContainer_PERS* 
ScintWaveformContainerCnv::createPersistent (ScintWaveformContainer* transCont) {
  ATH_MSG_DEBUG("ScintWaveformContainerCnv::createPersistent()");

  ScintWaveformContainerCnv_PERS converter;

  ScintWaveformContainer_PERS* persObj(nullptr);
  persObj = converter.createPersistent( transCont, msg() );
  return persObj;
}

ScintWaveformContainer* 
ScintWaveformContainerCnv::createTransient() {
  ATH_MSG_DEBUG("ScintWaveformContainerCnv::createTransient()");

  static const pool::Guid p0_guid("344d904d-6338-41f1-94ee-ea609ea4ae44");
  ScintWaveformContainer* trans(0);

  // Check for GUID of each persistent type
  if ( compareClassGuid(p0_guid) ) {
    std::unique_ptr< ScintWaveformContainer_p0 > col_vect( poolReadObject< ScintWaveformContainer_p0 >() );

    ScintWaveformContainerCnv_p0 converter_p0;
    trans = converter_p0.createTransient( col_vect.get(), msg() );

  } else {

    // Didn't find a known type
    throw std::runtime_error("Unsupported persistent version of ScintWaveformContainer");
  }

  return trans;

}


