/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SCINTEVENTATHENAPOOLCNVDICT_H
#define SCINTEVENTATHENAPOOLCNVDICT_H

#include "ScintEventAthenaPool/ScintWaveform_p0.h"
#include "ScintEventAthenaPool/ScintWaveformContainer_p0.h"

#endif
