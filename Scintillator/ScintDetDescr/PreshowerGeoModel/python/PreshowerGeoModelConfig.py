# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentFactory import CompFactory
# from IOVDbSvc.IOVDbSvcConfig import addFoldersSplitOnline

def PreshowerGeometryCfg( flags ):
    from FaserGeoModel.GeoModelConfig import GeoModelCfg
    acc = GeoModelCfg( flags )
    geoModelSvc = acc.getPrimary()

    GeometryDBSvc = CompFactory.GeometryDBSvc
    acc.addService(GeometryDBSvc("ScintGeometryDBSvc"))

    PreshowerDetectorTool = CompFactory.PreshowerDetectorTool
    preshowerDetectorTool = PreshowerDetectorTool()

    preshowerDetectorTool.useDynamicAlignFolders = flags.GeoModel.Align.Dynamic
    geoModelSvc.DetectorTools += [ preshowerDetectorTool ]

    # if flags.GeoModel.Align.Dynamic:
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL1/ID","/Indet/AlignL1/ID",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL2/SCT","/Indet/AlignL2/SCT",className="CondAttrListCollection"))
    #     acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/AlignL3","/Indet/AlignL3",className="AlignableTransformContainer"))
    # else:
    #     if (not flags.Detector.SimulatePreshower) or flags.Detector.OverlayPreshower:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align",className="AlignableTransformContainer"))
    #     else:
    #         acc.merge(addFoldersSplitOnline(flags,"INDET","/Indet/Onl/Align","/Indet/Align"))

    # if flags.Common.Project is not "AthSimulation": # Protection for AthSimulation builds
    #     if (not flags.Detector.SimulatePreshower) or flags.Detector.OverlayPreshower:
    #         from PreshowerConditionsAlgorithms.PreshowerConditionsAlgorithmsConf import PreshowerAlignCondAlg
    #         preshowerAlignCondAlg = PreshowerAlignCondAlg(name = "PreshowerAlignCondAlg",
    #                                             UseDynamicAlignFolders = flags.GeoModel.Align.Dynamic)
    #         acc.addCondAlgo(preshowerAlignCondAlg)
    #         from PreshowerConditionsAlgorithms.PreshowerConditionsAlgorithmsConf import PreshowerDetectorElementCondAlg
    #         preshowerDetectorElementCondAlg = PreshowerDetectorElementCondAlg(name = "PreshowerDetectorElementCondAlg")
    #         acc.addCondAlgo(preshowerDetectorElementCondAlg)

    return acc
